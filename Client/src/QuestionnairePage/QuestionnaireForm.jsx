import React from 'react';
import { Input, FormGroup, Label, FormFeedback, Button } from 'reactstrap';
import { AvForm } from 'availity-reactstrap-validation';
import { QuestionTypeInputs } from '../_components'

export const ReactstrapAvailityForm = ({questions, onValidSubmit, onChange, onInvalidSubmit}) => (
 
<div>
<h3>Questionnaire</h3>
  <AvForm onValidSubmit={onValidSubmit} onInvalidSubmit={onInvalidSubmit}>

    {questions.map((question, index) => (
        <div key={index}>
          <QuestionTypeInputs question={question} onChange={onChange}></QuestionTypeInputs>
        </div>

    ))}
  
    <Button type="submit">Submit</Button>
</AvForm>
</div>

);





// export const QuestionnaireFormik = ({questions, onSubmit, onChange}) => (
 
//   <div>
//     <h3>Questionnaire</h3>
//      <Formik
//        initialValues= {{questionAnswers : questions}} 
//        onSubmit={values => onSubmit(values)} 
     
       
//        render={({ values }) => (
//          <Form>
//          <FieldArray
//              name="questionAnswers"
//              render={arrayHelpers => (
//              <div>
//                  {values.questionAnswers.map((friend, index) => (
//                  <div key={index}>
//                      <Field type="text" 
//                      id={friend.id} 
//                      name={`questions[${index}]name`}
//                      label={friend.Question}
//                      component={renderQuestionType} 
//                      placeholder={friend.placeholder}
//                      onChange={onChange} />
//                  </div>
//                  ))}
               
//              </div>
//              )}
//          />
//           <button type="submit">Submit</button>
//          </Form>
//        )}
//      />
//    </div>
//  );