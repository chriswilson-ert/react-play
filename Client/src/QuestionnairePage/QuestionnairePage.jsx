import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { ReactstrapAvailityForm } from './QuestionnaireForm';
import { questionnaireActions } from '../_actions';

class QuestionnairePage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            questions : [
                { id: "q1", name: "q1n", type:"text", placeholder:"please enter", Question:"This is Q1", answer:"" },
                { id: "q2", name: "q2n", type:"text", placeholder:"please enter", Question:"This is Q2", answer:"" },
                { id: "q3", name: "q3n", type:"text", placeholder:"please enter", Question:"This is Q3", answer:"" },
                { id: "q4", name: "q4n", type:"select", options:[{ name:'a'}, {name:'b'}, {name:'c'}], Question:"This is Q4", answer:"" }
              ],
            submitted: false,
            questionnaire :''

        };

        this.handleChange = this.handleChange.bind(this);
        this.onValidSubmit = this.onValidSubmit.bind(this);
        this.onInvalidSubmit = this.onValidSubmit.bind(this);
    }
    

    
    handleChange(event) {
        let questions = this.state.questions.slice();
        for(let i in questions){
            if(questions[i].id == event.target.id){
                questions[i].answer = event.target.value;
                this.setState ({questions});
                break;
            }
        }
    }
       

    onValidSubmit (event, values)  {

        var assessment = { QuestionAnswers : JSON.stringify(values)};
        this.props.dispatch(questionnaireActions.submitQuestionnaire(assessment));
     };
    
    

      onInvalidSubmit(event, errors, values)  {
        this.setState({errors, values});
      };


    render(){
        return(
            <div>
            <ReactstrapAvailityForm questions={this.state.questions}
                onChange={this.handleChange} 
                onInvalidSubmit={this.onInvalidSubmit} 
                onValidSubmit={this.onValidSubmit} />
            </div>
       )
    }
}




const connectedQuestionnairePage = connect()(QuestionnairePage);
export { connectedQuestionnairePage as QuestionnairePage };
