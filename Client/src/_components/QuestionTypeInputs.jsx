import React from 'react';
import { AvForm, AvField, AvFeedback, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { Label, Button } from 'reactstrap';

class QuestionTypeInputs extends React.Component {
    constructor(props) {
        super(props);
        
     
        
      }
    
       render(){
      
        const {question} = this.props;

      if (question.type === 'text') {
        return (
          (
            <AvGroup>
              <Label for={question.name}>{question.Question}</Label>
              <AvInput name={question.name} id={question.id} type={question.type} required onChange={this.props.onChange}  /> 
              <AvFeedback>Answer it please</AvFeedback>
            </AvGroup>
           
          ) 
        ) 
        
      } else if (question.type === 'select') {
       
        return (
            <AvGroup>
              <Label for={question.name}>{question.Question}</Label>
                <AvInput type="select" name={question.name} onChange={this.props.onChange} id={question.id} >
                    {question.options.map((option, index) => {
                    return <option>{option.name}</option>
                    })}
                </AvInput>
                <AvFeedback>Select it please</AvFeedback>
            </AvGroup>
        )
      } else {
        return <div>Type not supported.</div>
      
    }
  }
}

export { QuestionTypeInputs };