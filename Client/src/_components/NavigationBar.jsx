import React, {Component} from 'react'

import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';


class NavigationBar extends Component {

    constructor(props) {
        super(props);
    
        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
          collapsed: true
        };
      }
    
      toggleNavbar() {
        this.setState({
          collapsed: !this.state.collapsed
        });
      }


    render(){
        return (
            <Navbar color="faded" light>
            <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
            <NavbarBrand href="/" className="mr-auto">study</NavbarBrand>
            <Collapse isOpen={!this.state.collapsed} navbar>
              <Nav navbar>
                 <NavItem>
                <NavLink href="/">Home</NavLink>
                </NavItem>
                <NavItem>
                <NavLink href="/q">Questionnaires</NavLink>
                </NavItem>
                <NavItem>
                <NavLink href="/login">Logout</NavLink>
                </NavItem>
              </Nav>
            </Collapse>
          </Navbar>
        )
    }
}

export default NavigationBar;

