import React from 'react';
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import { history } from '../_helpers';
import { alertActions } from '../_actions';
import { PrivateRoute, Header, Footer} from '../_components';
import { HomePage } from '../HomePage';
import { LoginPage } from '../LoginPage';
import { RegisterPage } from '../RegisterPage';
import { QuestionnairePage } from '../QuestionnairePage';

import {Container, Alert} from 'reactstrap';


class App extends React.Component {
    constructor(props) {
        super(props);

        const { dispatch } = this.props;
        history.listen((location, action) => {
            // clear alert on location change
            dispatch(alertActions.clear());
        });

        this.state = { visible: true};
        this.onDismiss = this.onDismiss.bind(this);
    }

    onDismiss() {

        this.setState({ visible: false });
      }

    render() {
        const { alert } = this.props;
        return (
            <Container className="App">
                  <Header /> 
                  <div className="col-sm-8 col-sm-offset-2">
                        {alert.message &&
                            // <div className={`alert ${alert.type}`} fade={true}>{alert.message}</div>
                            <Alert color={alert.type} isOpen={this.state.visible} toggle={this.onDismiss}>{alert.message}</Alert>
                        }
                    </div> 
                    <Router history={history}>
                        <div>
                            <PrivateRoute exact path="/" component={HomePage} />
                            <Route path="/login" component={LoginPage} />
                            <Route path="/register" component={RegisterPage} />
                            <Route path="/q" component={QuestionnairePage} />
                        </div>
                    </Router>
                 <Footer />
            </Container>                  
        )
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 