import config from 'config';
import { authHeader } from '../_helpers';

export const studyService = {
    activateStudyConfiguration,

};

function activateStudyConfiguration(activate) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 
        'APIKey': '9F5F459C-DAE4-4F98-AC7D-D79A38BD2F3A',
        'Exco-Target-Url':'https://apiengagement.int.ert.com/api/Study/ActivateStudyConfiguration' },
        body: JSON.stringify({ 'ActivationCode' : 'A6WK9-' })
    };

    var localStudyData = localStorage.getItem('studyData');
    if(localStudyData !== null  )
    {
      var obj = JSON.parse(localStudyData);
      return Promise.resolve(obj);
    }
    else
    {
    return fetch(`https://elmt.int.ert.com/WebAppHost/proxy.aspx`, requestOptions)
        .then(handleResponse)
        .then(study => {
            
            localStorage.setItem("studyData", JSON.stringify(study));
            
            return study;
        });
      }

   
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            alert(error);
            return Promise.reject(error);
        }

       return data;
    });
}




function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
    localStorage.removeItem('studyData');
}


