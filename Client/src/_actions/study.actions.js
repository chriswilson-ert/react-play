import { studyConstants } from '../_constants';
import { studyService } from '../_services';
import { alertActions } from './';


export const studyActions = {
    getStudyConfiguration
};

function getStudyConfiguration(activationCode) {
    return dispatch => {
        dispatch(request({ activationCode }));

        studyService.activateStudyConfiguration(activationCode)
            .then(
                study => { 
                    
                    dispatch(success(study));

                    
                    dispatch(alertActions.success('Activation done!'));
                
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };



    //alert(JSON.stringify(assessment));

    function request(study) { return { type: studyConstants.STUDYACTIVATION_REQUEST, study } }
    function success(study) {  return { type: studyConstants.STUDYACTIVATION_SUCCESS, study } }
    function failure(error) { return { type: studyConstants.STUDYACTIVATION_FAILURE, error } }
}




