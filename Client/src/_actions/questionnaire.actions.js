import { questionnaireConstants } from '../_constants';
import { questionnaireService } from '../_services';
import { alertActions } from './';


export const questionnaireActions = {
    submitQuestionnaire
};

function submitQuestionnaire(questionnaire) {
    return dispatch => {
        dispatch(request({ questionnaire }));

        questionnaireService.saveQuestionnaire(questionnaire)
            .then(
                questionnaire => { 
                    
                    dispatch(success(questionnaire));

                    
                    dispatch(alertActions.success('Save Complete!'));
                
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };



    //alert(JSON.stringify(assessment));

    function request(questionnaire) { return { type: questionnaireConstants.QUESTIONNAIRESAVE_REQUEST, questionnaire } }
    function success(questionnaire) {  return { type: questionnaireConstants.QUESTIONNAIRESAVE_SUCCESS, questionnaire } }
    function failure(error) { return { type: questionnaireConstants.QUESTIONNAIRESAVE_FAILURE, error } }
}




