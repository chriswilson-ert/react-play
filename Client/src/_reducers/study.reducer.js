import { studyConstants } from '../_constants';

export function study(state = {}, action) {
  
  switch (action.type) {
    case studyConstants.STUDYACTIVATION_REQUEST:
      return {loading :true};
    case studyConstants.STUDYACTIVATION_SUCCESS:
      return  action.study;//alert(action.assessment.StudyInstanceId); 
    case studyConstants.STUDYACTIVATION_FAILURE:
      return {};
    default:
      return state
  }
}