import { questionnaireConstants } from '../_constants';

export function questionnaire(state = {}, action) {
  
  switch (action.type) {
    case questionnaireConstants.QUESTIONNAIRESAVE_REQUEST:
      return {loading :true};
    case questionnaireConstants.QUESTIONNAIRESAVE_SUCCESS:
      return  action;//alert(action.assessment.StudyInstanceId); 
    case questionnaireConstants.QUESTIONNAIRESAVE_FAILURE:
      return {};
    default:
      return state
  }
}