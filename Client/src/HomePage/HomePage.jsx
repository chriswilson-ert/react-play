import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { studyActions } from '../_actions';

class HomePage extends React.Component {
    componentDidMount() {
        this.props.dispatch(studyActions.getStudyConfiguration("A6WK9-"));
    }

    render() {
        const { user, study } = this.props;
        
        return (
            <div>
                <div className="pull-right">logged in {user.firstName}</div>
                <br /><br />
                <h3>Assessments</h3>
         
              
                {study.loading && <em>Activating the study...</em>}   
                
   
                    {study.Questionnaires && study.Questionnaires.map(((qws, index) =>
        
                        <div key={qws.Id}>
                        {qws.Name &&
                            qws.Name.map((q,i) => (
                            
                                q.IsPrimary == true ?
                                
                                    <div key={q.Id}>
                                         <Link to="/q" className="btn btn-link">{q.Content}</Link>
                                    </div>
                                :
                                null
                            ))
                        }
                        </div>
                    ))} 
            



                
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { authentication, study } = state;
    const { user } = authentication;
   
    
    return {
        user,
        study
       
    };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export { connectedHomePage as HomePage };