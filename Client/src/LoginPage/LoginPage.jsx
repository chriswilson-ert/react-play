import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../_actions';
import {
    Container, Col, Form,
    FormGroup, Label, Input,
    Button, FormText, FormFeedback,
  } from 'reactstrap';

 

class LoginPage extends React.Component {
    constructor(props) {
        super(props);

        // reset login status
        this.props.dispatch(userActions.logout());

        this.state = {
            username: '',
            password: '',
            submitted: false,
            validate: {
                usernameState: '',
              }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidUpdate() {
       //alert('yep');
        //this.props.dispatch(assessmentActions.getStudyConfigurationFake("A6WK9-"));
    }

  


    handleChange(e) {
        this.setState({[e.target.name]: e.target.value})
    }

    async  changeAndValidate (event) {
        const { target } = event;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        await this.setState({
            [ event.target.name ]: value,
        });
    }


     
   blurred(e)
   {
        const { validate } = this.state
        validate.usernameState = 'has-success'
        this.setState({ validate })
   }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username, password } = this.state;
        const { dispatch } = this.props;
        if (username && password) {
            dispatch(userActions.login(username, password));
        }
    }

    render() {
        const { loggingIn } = this.props;
        const { username, password, submitted } = this.state;
        return (
            <div>
                <h2>Login</h2>
                <Form className="form" onSubmit={ (e) => this.handleSubmit(e) }>
          <Col>
            <FormGroup>
              <Label>Email</Label>
              <Input
                type="username"
                name="username"
                id="idUsername"
                placeholder="enter your username"
                value={ username }
                valid={ this.state.validate.usernameState === 'has-success' }
                invalid={ this.state.validate.usernameState === 'has-danger' }
                onChange={ (e) => { this.changeAndValidate(e)}}
                onBlur={(e)=>{this.blurred(e)}}
              />

              <FormFeedback valid>
                All good
              </FormFeedback>
              <FormFeedback invalid>
               Issue! 
              </FormFeedback>
        
            </FormGroup>
          </Col>
      

          <Col>
            <FormGroup>
              <Label for="examplePassword">Password</Label>
              <Input
                type="password"
                name="password"
                id="idPassword"
                placeholder="********"
                value={ password }
                onChange={ (e) => this.handleChange(e) }
              />
            </FormGroup>
          </Col>
          <Button>Submit</Button>
          <Link to="/register" className="btn btn-link">Register</Link>
          {loggingIn && <div>++ logging in ++</div>}
        </Form>
        
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage }; 