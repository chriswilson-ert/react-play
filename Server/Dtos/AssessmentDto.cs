namespace WebApi.Dtos
{
    public class AssessmentDto
    {
        public int Id { get; set; }
        public string QuestionAnswers { get; set; }
    }
}