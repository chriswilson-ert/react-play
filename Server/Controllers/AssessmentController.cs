﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using WebApi.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using WebApi.Services;
using WebApi.Dtos;
using WebApi.Entities;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class AssessmentController : ControllerBase
    {
        private IAssessmentService _assessmentService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public AssessmentController(
            IAssessmentService assessmentService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _assessmentService = assessmentService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        
        [AllowAnonymous]
        [HttpPost("Submit")]
        public IActionResult Submit([FromBody]AssessmentDto assessmentDto)
        {
            try 
            {
                var assessment = _mapper.Map<Assessment>(assessmentDto);
                _assessmentService.Submit(assessment);
                return Ok(assessment);
            } 
            catch(AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }


        [AllowAnonymous]
        [HttpPost("Submitss")]
        public IActionResult Submit(string assessment)
        {
            try
            {
                var ass = new Assessment { QuestionAnswers = assessment };
                _assessmentService.Submit(ass);
                return Ok(assessment);
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }


        [HttpGet]
        public IActionResult GetAll()
        {
            var assessments = _assessmentService.GetAll();
            var assessmentDtos = _mapper.Map<IList<AssessmentDto>>(assessments);
            return Ok(assessmentDtos);
        }

        
    }
}
