﻿
namespace WebApi.Entities
{
    public class Assessment
    {
        public int Id { get; set; }
        public string QuestionAnswers { get; set; }
    }
}
