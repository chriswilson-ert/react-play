﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{

    public interface IAssessmentService
    {

        Assessment Submit(Assessment assessment);
        IEnumerable<Assessment> GetAll();
    }

    public class AssessmentService : IAssessmentService
    {
        private DataContext _context;

        public AssessmentService(DataContext context)
        {
            _context = context;
        }

        public Assessment Submit(Assessment assessment)
        {
            _context.Assessments.Add(assessment);
            _context.SaveChanges();
            return assessment;
        }

        public IEnumerable<Assessment> GetAll()
        {
            return _context.Assessments;
        }


    }
}